# PRISSIV Project

The goal of this project is to build machine learnign and deep learning models for the automatic detection of step kinematics (during running or walking) from a single inertial measurement unit (IMU) sensor placed at the thorax level.

The [Movesense sensors](https://www.movesense.com/movesense-active/) are used in this project. 

### Dependencies

It is assumed you have Python 3.8 (apparently 3.9 does not work very well for Tf) installed on your machine. 

## Getting Started

* Clone the repository on a local position

```bash
git clone https://andrea_zignoli@bitbucket.org/andrea_zignoli/prissiv.git
cd prissiv
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

* For R, make sure you have tensorflow installed at level system

```bash
pip install tensorflow
```

You might find additional useful information about this installation process [here](https://tensorflow.rstudio.com/installation/) and [here](https://stackoverflow.com/questions/63220597/python-in-r-error-could-not-find-a-python-environment-for-usr-bin-python). 


### Json file data structure

```
Subject
      |
      -- Trial 1
      -- Trial 2
      ..
      -- Trial N
                |
                -- Thorax
                        |
                        -- Accelerometer
                        -- Gyro
                        -- Magnetometer
                        -- Heart rate
                -- Hip
                      |
                      -- Accelerometer
                      -- Gyro
                      -- Magnetometer
                -- Foot
                      |
                      -- Accelerometer
                      -- Gyro
                      -- Magnetometer
                -- Foot
                      |
                      -- Accelerometer
                      -- Gyro
                      -- Magnetometer
                -- Optogait
                          |
                          -- Contact time
                          -- Intermediate
```

## Project structure

Please check the project structure on [Miro](https://miro.com/app/board/o9J_lyfD5qs=/).

## Shiny web app 

The inference Web App is hosted on Shiny servers and can be used for inference at [Shiny Web App](https://andreazignoli.shinyapps.io/view_data/).

## Wandb training and testing progress

To check the progress of the trainig/testing process you can access the [Wandb project page](https://wandb.ai/andrea_zignoli/prissiv/overview?workspace=user-andrea_zignoli).

## Authors

Contributors names and contact info

* [Andrea Zignoli](https://www.researchgate.net/profile/Andrea-Zignoli) (project lead, model development)
* [Laurent Mourot](https://www.researchgate.net/profile/Laurent-Mourot) (project lead, data acquisition)
* Nicola Peghini (model deployment)
* Antoine Godin (data acquisition)

## Version History

* 0.2
    * Various bug fixes and optimizations
    * See [commit change]() or See [release history]()
* 0.1
    * Initial Release

## License

This project is licensed under the MIT License - see the license.txt file for details

## Acknowledgments

This readme file was created starting from a [awesome-readme template](https://github.com/matiassingers/awesome-readme).