import pandas as pd
from os import walk
from os.path import join
import matplotlib.pyplot as plt
import numpy as np
import json as js
import configparser

# Read local file `config.ini`.
config = configparser.ConfigParser()
config.read('config.ini')

# number of points (reduce memory load)
cut_limit = config.getint('SIGNALS', 'cut_limit')

# file folder
file_path = config['DATAFILES']['file_path']
# output directory
out_dir = config['DATAFILES']['out_dir']

# start 
dirpath, dirnames, filenames = next(walk(file_path))

for dir in dirnames:
    data_dict = {}
    _, dir_names, file_names = next(walk(join(file_path, dir)))
    optogait_converted_found = False
    movesense_found = False
    optogait_dir = 'Optogait normal'

    for dir_names_ in dir_names:
        if dir_names_ == 'Movesense':
            movesense_found = True
        if dir_names_ == 'Optogait normal' or dir_names_ == 'Optogait':
            _, sub_dir_names, sub_file_names = next(walk(join(file_path, dir, dir_names_)))
            for sub_dir_names_ in sub_dir_names:
                if sub_dir_names_ == 'Optogait (converted file)':
                    optogait_converted_found = True

    # if you found both the dir you are interested in:
    if movesense_found and optogait_converted_found:
        data_dict[dir] = {}
        _, movesense_trial_dir, _ = next(walk(join(file_path, dir, 'Movesense')))

        for movesense_trial_dir_ in movesense_trial_dir:
            _, _, IMU_files = next(walk(join(file_path, dir, 'Movesense', movesense_trial_dir_)))
            condition_name = movesense_trial_dir_.split('_')[0].split(' ')[-1].replace('+', '')

            if condition_name not in data_dict[dir]:
                data_dict[dir][condition_name] = {}
            for IMU_files_ in IMU_files:
                my_file = join(file_path, dir, 'Movesense', movesense_trial_dir_, IMU_files_)
                IMUID = IMU_files_.split('_')[1]

                if IMUID not in data_dict[dir][condition_name]:
                    data_dict[dir][condition_name][IMUID] = {}

                df = pd.DataFrame()
                df = pd.read_csv(my_file)
                # create smaller df
                df = df[0:cut_limit]

                data_dict[dir][condition_name][IMUID][IMU_files_.split('_')[2]] = df.to_dict(
                    'list')

        try:
            _, _, optogait_trial_files = next(walk(join(file_path, dir, 'Optogait normal', 'Optogait (converted file)')))
            optogait_dir = 'Optogait normal'
        except:
            _, _, optogait_trial_files = next(
                walk(join(file_path, dir, 'Optogait', 'Optogait (converted file)')))
            optogait_dir = 'Optogait'

        for optogait_trial_files_ in optogait_trial_files:
            condition_name = optogait_trial_files_.split('_')[0].split(' ')[-1]

            if condition_name not in data_dict[dir]:
                data_dict[dir][condition_name] = {}

            my_file = join(file_path, dir, optogait_dir, 'Optogait (converted file)', optogait_trial_files_)

            df = pd.read_csv(my_file, delimiter="\t")
            # create smaller df
            df = df[0:cut_limit]
            data_dict[dir][condition_name]['Optogait'] = df.to_dict('list')

        # create labels after aligning the signals
        subject_name_key = list(data_dict.keys())
        for trial_name in data_dict[subject_name_key[0]].keys():
            # subject_name_key = list(data_dict.keys())
            # subject_trial_key = list(data_dict[subject_name_key[0]].keys())
            try:
                print('Processing ', trial_name, ' for ', dir)
                # trial_name = subject_trial_key[0]
                sensor_ID_list = list(data_dict[subject_name_key[0]][trial_name].keys())
                # find the peak index
                # TODO : implement threshold for spike detection (now is hardcoded 5*g)
                # TODO: look for the right foot sensor

                sensor_ID_list.remove('Optogait')
                first_index = np.Inf
                for IMU_sensor in sensor_ID_list:
                    z_array = np.asarray(data_dict[subject_name_key[0]][trial_name][IMU_sensor]['acc']['z'])
                    try:
                        index_ = np.where(np.abs(z_array) > 50)[0][0] # close to sat
                    except:
                        index_ = np.Inf
                    if index_ < first_index:
                        first_index = index_
                        selected_ID_sensor = IMU_sensor

                # select the first peak of the z leading
                leading_z = np.array(data_dict[subject_name_key[0]][trial_name][selected_ID_sensor]['acc']['z'])
                time_array = np.array(data_dict[subject_name_key[0]][trial_name][selected_ID_sensor]['acc']['timestamp'])
                time_series = pd.Series((time_array - time_array[0])/1000) # seconds
                time_series = time_series.interpolate()
                time_array = time_series.values - time_series.values[first_index]

                # You have a time delay between Movesense and Optogait
                optogait_time = np.asarray(data_dict[subject_name_key[0]][trial_name]['Optogait']['Intermediate']) - \
                                data_dict[subject_name_key[0]][trial_name]['Optogait']['Intermediate'][0]
                contact_time = np.asarray(data_dict[subject_name_key[0]][trial_name]['Optogait']['ContactTime'])

                # definition of labels
                right_left_steps = np.zeros(shape=time_array.shape)
                right_left_contact = np.zeros(shape=time_array.shape)
                label = - np.ones(shape=time_array.shape)

                for i in np.arange(0, len(optogait_time[:-1]), 2):
                    right_left_steps[((time_array > optogait_time[i]) & (time_array < optogait_time[i + 1]))] = 1
                    right_left_contact[((time_array > optogait_time[i]) & (time_array < optogait_time[i] + contact_time[i]))] = 2
                    label[
                        ((time_array > optogait_time[i]) & (time_array < optogait_time[i] + contact_time[i]))] = 1

                for i in np.arange(1, len(optogait_time[:-1]), 2):
                    right_left_steps[((time_array > optogait_time[i]) & (time_array < optogait_time[i + 1]))] = -1
                    right_left_contact[
                        ((time_array > optogait_time[i]) & (time_array < optogait_time[i] + contact_time[i]))] = -2
                    label[
                        ((time_array > optogait_time[i]) & (time_array < optogait_time[i] + contact_time[i]))] = 1

                data_dict[dir][trial_name]['label'] = label.tolist()
                # FIXME: there is no such a thing like common time array for the sensors
                data_dict[dir][trial_name]['initial_time_delay'] = time_series.values[first_index]
            except:
                print('Skipping ', trial_name, ' for ', dir)
                pass

    if movesense_found and optogait_converted_found:
        print('creating the dict for ', dir)
        with open(join(out_dir, dir.replace('(', '').replace(')', '').replace(' ', '') + 'test.json'), 'w') as outfile:
            js.dump(data_dict, outfile, sort_keys=True, indent=4)






