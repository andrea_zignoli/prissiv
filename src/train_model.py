from os import walk
from os.path import join
import matplotlib.pyplot as plt
import numpy as np
import json as js
import os
import random
import tensorflow as tf
from ModelCreator import CNN_model
from ModelCreator import MLP_model, LSTM_model
from DataSetPreparation import data_set_preparation
import configparser
import wandb

# read local configuration file
config = configparser.ConfigParser()
config.read('config.ini')

os.environ['WANDB_MODE'] = config['WANDB']['mode']
os.environ['WANDB_API_KEY'] = config['WANDB']['API_KEY']
model_dir = config['WANDB']['model_dir']
entity = config['WANDB']['entity']
project_name = config['WANDB']['project_name']
wandb.init(project=project_name, entity=entity)

# model details
epochs = config.getint('MODEL', 'epochs')
lr = config.getfloat('MODEL', 'lr')
batch_size = config.getint('MODEL', 'batch_size')
wandb_config = wandb.config
wandb_config.learning_rate = lr
cls = config.getint('MODEL', 'cls')

thorax_only = config.getboolean('MODEL', 'Thorax')
if thorax_only:
    n_sensors = 3
else:
    n_sensors = 12

# Hz
fc = config.getfloat('SIGNALS', 'fc')
time_seq = config.getfloat('MODEL', 'time_seq')
time_stride = config.getfloat('MODEL', 'time_stride')
seq_length = int(time_seq * fc)
stride_length = int(time_stride * fc)
# please change this line if you add/remove directions from IMUs
n_input = 6

# start
out_dir = config['DATAFILES']['out_dir']

dirpath, dirnames, filenames = next(walk(out_dir))
# remove the hidden files
filenames = [i for i in filenames if i[-5:] == '.json']
random.shuffle(filenames)

# split the list between training and validation files
train_filename_list = filenames[:int(len(filenames)*0.8)]
test_filename_list = filenames[int(len(filenames)*0.8):]

# load and balance json data dict
train_ds = data_set_preparation()
train_ds.load_json(dirpath, dirnames, train_filename_list)
X_train, y_train = train_ds.windowing(fc_=fc, time_seq_=time_seq, time_stride_=time_stride, cls_=cls, thorax_only_=thorax_only, gyro_=True)
# X_train, y_train = train_ds.balance(X_train, y_train)

# load json data dict
test_ds = data_set_preparation()
test_ds.load_json(dirpath, dirnames, test_filename_list)
X_test, y_test = test_ds.windowing(fc_=fc, time_seq_=time_seq, time_stride_=time_stride, cls_=cls, thorax_only_=thorax_only, gyro_=True)

# initiate the model
model_type = config['MODEL']['type']
if model_type == 'dense':
    model = MLP_model(cls, n_input)
if model_type == 'LSTM':
    model = LSTM_model(cls, n_input)
if model_type == 'CNN':
    model = CNN_model(cls, n_input)

optimizer = tf.keras.optimizers.Adam(learning_rate=lr)
# loss_object = tf.keras.losses.CategoricalCrossentropy(from_logits=True)

def bounded_output(x, lower, upper):
    scale = upper - lower
    return scale * tf.nn.sigmoid(x) + lower

def loss_object(y_true, y_pred):
    MSE = tf.keras.losses.mean_squared_error(y_true, y_pred)
    # max SF 3 min SF 0.5
    bounded_SF = bounded_output(y_pred.numpy()[:, 0], -0.9, 1.6) + bounded_output(y_pred.numpy()[:, 2], -0.9, 1.6)
    bounded_DF = bounded_output(y_pred.numpy()[:, 1], -0.1, 0.2) + bounded_output(y_pred.numpy()[:, 3], -0.1, 0.2)
    return MSE + bounded_DF + bounded_SF

# @tf.function
def train_step(data, labels):

    with tf.GradientTape() as tape:
        logits = model(data, training=True)
        loss = loss_object(labels, logits)
    trainable_variables = model.trainable_variables
    gradients = tape.gradient(loss, trainable_variables)
    optimizer.apply_gradients(zip(gradients, trainable_variables))

    eq = tf.equal(labels, logits)
    accuracy = tf.reduce_mean(tf.cast(eq, tf.float32))
    return tf.reduce_mean(loss), accuracy

# @tf.function
def validation_step(data, labels):
    logits = model(data, training=False)
    loss = loss_object(labels, logits)
    eq = tf.equal(labels, logits)
    accuracy = tf.reduce_mean(tf.cast(eq, tf.float32))

    return tf.reduce_mean(loss), accuracy

step = 0
for epoch in range(epochs):

    ds_train = list(zip(X_train, y_train))
    random.shuffle(ds_train)
    X_train_shuffled, y_train_shuffled = list(zip(*ds_train))
    # schedule lr
    if epoch == 20:
        optimizer.lr = optimizer.lr * 0.1
    if epoch == 40:
        optimizer.lr = optimizer.lr * 0.1
    if epoch == 60:
        optimizer.lr = optimizer.lr * 0.1
    for t_ in range(0, len(y_train_shuffled), batch_size):
        data, labels = np.array(X_train_shuffled[t_:t_ + batch_size]), np.array(y_train_shuffled[t_:t_ + batch_size])
        # print(step, ' -- ', np.size(data), ' -- ', np.size(labels))
        train_done = False
        try:
            train_loss, train_accuracy = train_step(data, labels)
            train_done = True
        except:
            print('Training failed')
            break
            pass
        if step % 100 == 0 and train_done:
            print('STEP: ', step, ' BATCH LOSS: ',  train_loss.numpy(), ' BATCH ACCURACY: ',  train_accuracy.numpy())

            wandb.log({"train/epoch": epoch + 1, "train/loss": train_loss.numpy(),
                       "train/accuracy": train_accuracy.numpy(), "learning_rate": optimizer.lr.numpy()}, step=step)
        step += 1
    print('Start validation for Epoch {}'.format(epoch + 1))
    # validation phase

    # validation data
    ds_test = list(zip(X_test, y_test))
    random.shuffle(ds_test)
    X_test_shuffled, y_test_shuffled = list(zip(*ds_test))
    for v_ in range(0, len(y_test_shuffled), batch_size):
        data, labels = np.array(X_test_shuffled[v_:v_ + batch_size]), np.array(y_test_shuffled[v_:v_ + batch_size])
        test_done = False
        try:
            val_loss, val_accuracy = train_step(data, labels)
            test_done = True
        except:
            print('Testing failed')
            break
            pass

    if test_done:
        template = 'Epoch {}, Val Loss: {}'
        print('EPOCH: ', epoch, ' LOSS: ',  val_loss.numpy(), ' ACCURACY: ',  val_accuracy.numpy())
        wandb.log({"val/epoch": epoch, "val/loss": val_loss, "val/accuracy": val_accuracy}, step=step)
        inference = model(data, training=False)
        wandb.log({"Input data": wandb.plot.line_series(
            xs=np.arange(len(data[0, 1, :])).tolist(),
            ys=[data[0, 0, :].tolist(), data[0, 1, :].tolist(), data[0, 2, :].tolist()],
            keys=['Acc x', 'Acc y', 'Acc z'],
            title='Input',
            xname="Samples")})
        wandb.log({"Foot_1_plot_SF": wandb.plot.line_series(
            xs=np.arange(len(labels[:, 0])).tolist(),
            ys=[labels[:, 0].tolist(), inference[:, 0].numpy().tolist()],
            keys=['Labels 1', 'Inference 1'],
            title='Foot 1 SF',
            xname="Samples")})
        wandb.log({"Foot1_plot_DF": wandb.plot.line_series(
            xs=np.arange(len(labels[:, 0])).tolist(),
            ys=[labels[:, 1].tolist(), inference[:, 1].numpy().tolist()],
            keys=['Labels 2', 'Inference 2'],
            title='Foot 1 DF',
            xname="Samples")})
        wandb.log({"Foot_2_plot_SF": wandb.plot.line_series(
            xs=np.arange(len(labels[:, 0])).tolist(),
            ys=[labels[:, 2].tolist(), inference[:, 2].numpy().tolist()],
            keys=['Labels 1', 'Inference 1'],
            title='Foot 2 SF',
            xname="Samples")})
        wandb.log({"Foot_2_plot_DF": wandb.plot.line_series(
            xs=np.arange(len(labels[:, 0])).tolist(),
            ys=[labels[:, 3].tolist(), inference[:, 3].numpy().tolist()],
            keys=['Labels 2', 'Inference 2'],
            title='Foot 2 DF',
            xname="Samples")})

if model_type == 'dense':
    w = os.path.join(os.getcwd(), model_dir, 'weights_MPL.ckpt')
    m = os.path.join(os.getcwd(), model_dir, 'MLP_model')
    model.save_weights(w)
    model.save(m)

if model_type == 'LSTM':
    w = os.path.join(os.getcwd(), model_dir, 'weights_LSTM.ckpt')
    m = os.path.join(os.getcwd(), model_dir, 'LSTM_model')
    model.save_weights(w)
    model.save(m)

if model_type == 'CNN':
    w = os.path.join(os.getcwd(), model_dir, 'weights_CNN.ckpt')
    m = os.path.join(os.getcwd(), model_dir, 'CNN_model')
    model.save_weights(w)
    model.save(m)
