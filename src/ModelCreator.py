import tensorflow as tf

class LSTM_model(tf.keras.Model):
    def __init__(self):
        super(LSTM_model, self).__init__()

        # Shape [batch, time, features] => [batch, time, lstm_units]
        self.lstm = tf.keras.layers.LSTM(32, return_sequences=True)
        # Shape => [batch, time, features]
        self.d = tf.keras.layers.Dense(units=1)

    def call(self, inputs, training=None, mask=None):

        inputs = tf.reshape(inputs, (inputs.shape[0], inputs.shape[2], inputs.shape[1]))
        x = self.lstm(inputs)
        x = self.d(x)
        return tf.squeeze(x)

class MLP_model(tf.keras.Model):
    def __init__(self, n_classes, n_sensors):
        super(MLP_model, self).__init__()

        self.f = tf.keras.layers.Flatten()
        self.d0 = tf.keras.layers.Dense(32)
        self.d1 = tf.keras.layers.Dense(64)
        self.d2 = tf.keras.layers.Dense(n_classes)

    def call(self, inputs, training=None):
        x = self.f(inputs)
        x = self.d0(x)
        x = self.d1(x)
        x = self.d2(x)
        return x

    def build(self, input_shape):
        name = 'model'
        x = tf.keras.layers.Input(shape=input_shape[1:])
        y = tf.keras.Model(inputs=[x], outputs=self.call(x, training=False), name=name)
        return y.summary()

    def loadModel(self, model_path, n_classes, n_sensors):
        self.model = MLP_model(n_classes, n_sensors)
        self.load_weights(model_path)

class CNN_model(tf.keras.Model):
    def __init__(self, n_classes, n_input):
        super(CNN_model, self).__init__()
        # self.ps = int((n_sensors - 1) / 2)

        self.conv = tf.keras.layers.Conv2D(filters=32, kernel_size=(n_input, 12), strides=(1, 1), padding='VALID')
        self.bn = tf.keras.layers.BatchNormalization()

        self.conv2 = tf.keras.layers.Conv1D(filters=32, kernel_size=5, strides=3, padding='VALID')
        self.norm2 = tf.keras.layers.BatchNormalization()
        self.drop2 = tf.keras.layers.Dropout(0.2)

        self.maxpool2 = tf.keras.layers.MaxPooling1D(pool_size=4, strides=3, padding='VALID')

        self.f = tf.keras.layers.Flatten()
        self.d0 = tf.keras.layers.Dense(64)
        self.d1 = tf.keras.layers.Dense(n_classes)

    def call(self, inputs, training=None):

        x = tf.expand_dims(inputs, axis=-1)
        x = self.conv(x)
        x = self.bn(x, training)
        x = tf.keras.activations.relu(x)
        x = tf.squeeze(x, axis=1)

        x = self.conv2(x)
        x = self.norm2(x, training=training)
        x = self.drop2(x, training=training)

        x = self.maxpool2(x)

        x = self.f(x)
        x = self.d0(x)

        return self.d1(x)

    def build(self, input_shape):
        name = 'model'
        x = tf.keras.layers.Input(shape=input_shape[1:])
        y = tf.keras.Model(inputs=[x], outputs=self.call(x, training=False), name=name)
        # y.summary()
        return y

    def loadModel(self, model_path, n_classes, n_sensors):
        self.model = CNN_model(n_classes, n_sensors)
        self.load_weights(model_path)