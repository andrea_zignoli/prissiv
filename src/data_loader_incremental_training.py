import pandas as pd
from os.path import join
from os import walk
import configparser
import json as js

# Read local file `config.ini`.
config = configparser.ConfigParser()
config.read('config.ini')

# file folder
file_path = config['INCREMENTALTRAINING']['file_path']
# output directory
out_dir = config['INCREMENTALTRAINING']['out_dir']

for dirs in next(walk(file_path))[1]:
    data_dict = {}
    data_dict[dirs] = {}
    data_dict[dirs]['random_condition'] = {}
    data_dict[dirs]['random_condition']['initial_time_delay'] = 0
    data_dict[dirs]['random_condition']['label'] = {}
    dirpath_, dirnames_, filenames_ = next(walk(join(file_path, dirs)))
    filenames_ = [i for i in filenames_ if i[-4:] == '.csv']
    for files in filenames_:
        print(files)
        # load and balance json data dict
        str_ = files.split('_')
        if not str_[1] in data_dict[dirs]['random_condition'].keys():
            data_dict[dirs]['random_condition'][str_[1]] = {}
        tmp_dict = pd.read_csv(join(file_path, dirs, files))
        if str_[2] == 'acc':
            if not 'acc' in data_dict[dirs]['random_condition'][str_[1]].keys():
                data_dict[dirs]['random_condition'][str_[1]]['acc'] = {}
            for coord_ in tmp_dict.columns.to_list():
                data_dict[dirs]['random_condition'][str_[1]]['acc'][coord_] = tmp_dict[coord_].to_list()
        if str_[2] == 'gyro':
            if not 'gyro' in data_dict[dirs]['random_condition'][str_[1]].keys():
                data_dict[dirs]['random_condition'][str_[1]]['gyro'] = {}
            for coord_ in tmp_dict.columns.to_list():
                data_dict[dirs]['random_condition'][str_[1]]['gyro'][coord_] = tmp_dict[coord_].to_list()
        if str_[2] == 'heartRate':
            if not 'heartRate' in data_dict[dirs]['random_condition'][str_[1]].keys():
                data_dict[dirs]['random_condition'][str_[1]]['heartRate'] = {}
                data_dict[dirs]['random_condition'][str_[1]]['heartRate'] = tmp_dict['average'].to_list()

    print('creating the dict for ', dirs)
    with open(join(out_dir, dirs.replace(' ', '') + '.json'), 'w') as outfile:
        js.dump(data_dict, outfile, sort_keys=True, indent=4)
