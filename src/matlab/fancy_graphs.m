%% Preamble

clear all
clc
close all
addpath('data_sample');
addpath(genpath('functions'));

Figure_Settings

%%
foot_1_acc = readtable('foot_1_acc_stream.csv');
foot_2_acc = readtable('foot_2_acc_stream.csv');
thorax_acc = readtable('thorax_acc_stream.csv');

%% 
foot_1_gyro = readtable('foot_1_gyro_stream.csv');
foot_2_gyro = readtable('foot_2_gyro_stream.csv');
thorax_gyro = readtable('thorax_gyro_stream.csv');

%% Plot for presentation

% final plot time index
t_end = 2000;

fig             = figure();
fig.Position    = [100, 100, 1200, 100];
hold on
plot(foot_1_acc.timestamp(1:10:t_end) - foot_1_acc.timestamp(1), foot_1_acc.y(1:10:t_end) * 20, ...
    'color', colForestGreen)
plot(foot_1_gyro.timestamp(1:t_end) - foot_1_gyro.timestamp(1), foot_1_gyro.y(1:t_end), ...
    'color', colCornFlowerBlue)
plot(foot_1_gyro.timestamp(1:t_end) - foot_1_gyro.timestamp(1), foot_1_gyro.z(1:t_end), ...
    'color', colDeepPink)

ax1 = gca();
ax1.XAxis.Visible = 'off';
ax1.YAxis.Visible = 'off';
% ax1.XTickLabel = [];

set(gca, 'color', 'none');
set(gcf, 'color', 'none');
export_fig('gg_plot', '-dpng', '-transparent', '-r300');