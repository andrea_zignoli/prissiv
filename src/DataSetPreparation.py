import numpy as np
from sklearn.preprocessing import MinMaxScaler
import random
import sys, os
import matplotlib.pyplot as plt

class data_set_preparation:
    def __init__(self):
        pass

    def load_json(self, dirpath, dirnames, filenames):
        from os.path import join
        import json as js
        import os
        data_dict = dict()
        for files_ in filenames:
            if files_.split('.')[-1] == 'json':
                print(files_)
                with open(join(dirpath, files_)) as infile:
                    tmp_dict = js.load(infile)
                    tmp_keys = list(tmp_dict.keys())
                    data_dict[tmp_keys[0]] = tmp_dict[tmp_keys[0]]
        self.data = data_dict

    def windowing(self, fc_=208, time_seq_=0.25, time_stride_=0.1, cls_=2, thorax_only_=True, gyro_=False):
        import pandas as pd
        import numpy as np
        from scipy.signal import butter, filtfilt, find_peaks
        from scipy import fftpack
        import heapq

        def lag_finder(y1, y2, sr):
            from scipy import signal
            import numpy as np
            n = len(y1)

            corr = signal.correlate(y1, y2, mode='same') / np.sqrt(
                signal.correlate(y1, y1, mode='same')[int(n / 2)] * signal.correlate(y2, y2, mode='same')[int(n / 2)])

            delay_arr = np.linspace(0, 0.5 * n / sr, n)
            delay = delay_arr[np.argmax(corr)]
            return delay

        def butter_lowpass_filter(data, cutoff, fs, order):
            nyq = fs/2
            normal_cutoff = cutoff / nyq
            # Get the filter coefficients
            b, a = butter(order, normal_cutoff, btype='low', analog=False)
            y = filtfilt(b, a, data)
            return y

        def find_freq(signal, fc):
            w = np.fft.fft(signal - np.mean(signal))
            freqs = np.fft.fftfreq(len(signal))
            idx = np.argmax(np.abs(w))
            # Hz!
            return np.abs(freqs[idx] * fc)

        def detect_RL_side(signal_1, signal_2, reference):

            # this function returns the side of the first signal
            peaks_1, _ = find_peaks(-signal_1, height=10, distance=100)
            peaks_2, _ = find_peaks(-signal_2, height=10, distance=100)
            ref_peaks_UP, _ = find_peaks(reference, height=10, distance=100)
            ref_peaks_DOWN, _ = find_peaks(-reference, height=10, distance=100)

            # The signal 1 is leading
            peaks_2 = peaks_2[peaks_2 > peaks_1[0]]
            ref_peaks_UP = ref_peaks_UP[ref_peaks_UP > peaks_1[0]]
            ref_peaks_DOWN = ref_peaks_DOWN[ref_peaks_DOWN > peaks_1[0]]

            tmp_side = []
            for i in np.arange(0, len(peaks_1)):
                try:
                    if ref_peaks_UP[i] > peaks_1[i] and ref_peaks_UP[i] < peaks_2[i]:
                        tmp_side.append('R')
                    else:
                        if ref_peaks_DOWN[i] > peaks_2[i] and ref_peaks_DOWN[i] < peaks_1[i]:
                            tmp_side.append('R')
                        else:
                            if ref_peaks_UP[i] > peaks_2[i] and ref_peaks_UP[i] < peaks_1[i]:
                                tmp_side.append('L')
                            else:
                                if ref_peaks_DOWN[i] > peaks_1[i] and ref_peaks_DOWN[i] < peaks_2[i]:
                                    tmp_side.append('L')

                except:
                    pass

            if len(set(tmp_side)) > 1:
                raise ValueError('A very specific bad thing happened.')

            if tmp_side[0] == 'L':
                here=0

            return tmp_side[0]

        def find_freq_from_peaks(signal, fc):
            signal = (signal - np.mean(signal))
            peaks, _ = find_peaks(- signal, height=10, distance=100)

            minor_peaks = list()
            Tc = list()
            for pk in peaks:
                # TODO: these are arbitrary numbers (peak distance and normalised freq of 1.4 and DF 0.35)
                m_peaks, _ = find_peaks(np.diff(signal)[pk:pk+80])
                try:
                    if max(m_peaks) > 20:
                        minor_peaks.append(max(m_peaks) + pk)
                        Tc.append(max(m_peaks) / fc)
                except:
                    pass

            SF = 1/(np.mean(np.diff(peaks))/fc) - 1.4
            DF = np.mean(Tc) / np.mean(np.diff(peaks)/fc) - 0.35

            return SF, DF

        data_dict = self.data
        X_dataset = list()
        y_dataset = list()
        fc = fc_
        time_seq = time_seq_
        time_stride = time_stride_
        cls = cls_
        thorax_only = thorax_only_
        seq_length = int(time_seq * fc)
        stride_length = int(time_stride * fc)
        gyro = gyro_
        n_sensors = 3
        # parameters number is equivalent to the input time series number
        n_pars = 6

        for subj_ in data_dict.keys():
            for cond_ in data_dict[subj_].keys():
                try:
                    parameters = []
                    for sensors_ in data_dict[subj_][cond_].keys():

                        if sensors_ not in ['Optogait', 'label', 'initial_time_delay']:
                            thorax_detected = False
                            if 'heartRate' in data_dict[subj_][cond_][sensors_].keys():
                                thorax_sensor_ID = sensors_
                                thorax_detected = True

                            if thorax_only and thorax_detected:
                                parameters = []
                                # TODO: take care of this, as we are removing x coord
                                foo_acc = ['x', 'y', 'z']
                                tmp_acc = []
                                for coord_ in foo_acc:
                                    if not 'timestamp' in coord_:
                                        # TODO : standardization, I think you are loosing details
                                        #  if you remove the averages
                                        time_array = np.array(data_dict[subj_][cond_][sensors_]['acc']['timestamp'])
                                        time_series = pd.Series((time_array - time_array[0]) / 1000)
                                        time_series = time_series.interpolate()
                                        time_array = time_series.values - data_dict[subj_][cond_]['initial_time_delay']
                                        tmp_index = np.where(time_array >= 0)[0][0]
                                        tmp_acc.append(coord_)
                                        scaler = MinMaxScaler()
                                        tmp_data = np.array(data_dict[subj_][cond_][sensors_]['acc'][coord_][tmp_index:])
                                        # normalization between -1 and 1 AFTER!
                                        data_raw = tmp_data / 9.8
                                        # data_raw_zero_mean = data_raw - np.mean(data_raw)
                                        # scaler.set_params(feature_range=(-1, 1))
                                        # scaler.fit(data_raw_zero_mean.reshape(-1, 1))
                                        # data_raw_ST = scaler.transform(data_raw_zero_mean.reshape(-1, 1))
                                        parameters.append(data_raw.reshape(-1, ))
                                if gyro:
                                    foo_gyro = ['x', 'y', 'z']
                                    tmp_gyro = []
                                    for coord_ in foo_gyro:
                                        if not 'timestamp' in coord_:
                                            # TODO : standardization
                                            time_array = np.array(data_dict[subj_][cond_][sensors_]['gyro']['timestamp'])
                                            time_series = pd.Series((time_array - time_array[0]) / 1000)
                                            time_series = time_series.interpolate()
                                            time_array = time_series.values - data_dict[subj_][cond_]['initial_time_delay']
                                            tmp_index = np.where(time_array >= 0)[0][0]
                                            tmp_gyro.append(coord_)
                                            scaler = MinMaxScaler()
                                            # TODO: noramlising with 500 deg/s
                                            tmp_data = np.array(data_dict[subj_][cond_][sensors_]['gyro'][coord_][tmp_index:])
                                            # normalization between -1 and 1 AFTER!
                                            data_raw = tmp_data / 50
                                            # TODO: please be sure to not remove all the averages
                                            #  since you are loosing details
                                            # data_raw_zero_mean = data_raw - np.mean(data_raw)
                                            # scaler.set_params(feature_range=(-1, 1))
                                            # scaler.fit(data_raw_zero_mean.reshape(-1, 1))
                                            # data_raw_ST = scaler.transform(data_raw_zero_mean.reshape(-1, 1))
                                            parameters.append(data_raw.reshape(-1, ))
                            else:
                                if not thorax_only_:
                                    for coord_ in data_dict[subj_][cond_][sensors_]['acc'].keys():
                                        if not 'timestamp' in coord_:
                                            parameters.append(
                                                np.array(data_dict[subj_][cond_][sensors_]['acc'][coord_][tmp_index:]))

                        if sensors_ in ['label'] and tmp_acc == foo_acc and tmp_gyro == foo_gyro:
                            # TODO: risky because you do not know
                            # FIXME: please let us check the TRUE sensor index
                            # labels WERE Optogait

                            # detect feet
                            mean_y_comp = list()
                            for sens_ in np.arange(0, n_sensors):
                                y_signal = data_dict[subj_][cond_][list(data_dict[subj_][cond_].keys())[sens_]]['acc']['y'][
                                tmp_index:]
                                y_signal = [0 if k < 60 else 1 for k in y_signal]
                                mean_y_comp.append(np.sum(y_signal))

                            feet_idx = heapq.nlargest(2, range(len(mean_y_comp)), mean_y_comp.__getitem__)
                            # FOOT 1 and 2

                            sensor_1 = data_dict[subj_][cond_][list(data_dict[subj_][cond_].keys())[feet_idx[0]]]['acc']['z'][
                                     tmp_index:]
                            sensor_2 = data_dict[subj_][cond_][list(data_dict[subj_][cond_].keys())[feet_idx[1]]]['acc']['z'][
                                     tmp_index:]

                            # THORAX (will be used together with feet to detect the right and left feet)
                            thorax_sensor_gyro_y = data_dict[subj_][cond_][thorax_sensor_ID]['gyro']['y'][tmp_index:]
                            thorax_sensor_acc_y = data_dict[subj_][cond_][thorax_sensor_ID]['acc']['y'][tmp_index:]

                            # OPTOGAIT
                            # labels = np.array(data_dict[subj_][cond_][sensors_][tmp_index:])
                            # you need to cut after min(map(len, parameters))
                            min_duration = min(map(len, parameters))
                            my_array = np.empty((n_pars, min(map(len, parameters))))
                            for i, par_ in enumerate(parameters):
                                my_array[i, :] = (par_[0:min(map(len, parameters))])
                            # FIXME: pretty bad names
                            parameters = my_array

                            # windowing on parameters
                            for i_ in range(0, np.shape(parameters)[1], stride_length):
                                if not np.shape(parameters[:, i_:i_ + seq_length])[-1] < seq_length:
                                    xN = np.zeros([len(parameters), len(np.arange(i_, i_ + seq_length))])
                                    for lines_ in np.arange(len(parameters)):
                                        x = parameters[lines_, i_:i_ + seq_length]
                                        xN[lines_, :] = 2 * (x - np.min(x))/(np.max(x) - np.min(x))-1
                                    input_clear = False
                                    if not np.isnan(xN).any():
                                        input_clear = True
                                    else:
                                        print('NAN in the input!!!!')

                                    l = []
                                    # 4 classes it means SF1, DF1, SF2, DF2
                                    if cls == 4:

                                        sensor_1F = butter_lowpass_filter(sensor_1[i_:i_ + seq_length], 10, fc_, 4)
                                        sensor_2F = butter_lowpass_filter(sensor_2[i_:i_ + seq_length], 10, fc_, 4)
                                        thorax_sensor_gyro_yF = butter_lowpass_filter(thorax_sensor_gyro_y[i_:i_ + seq_length], 10, fc_, 4)
                                        # thorax_sensor_acc_yF = butter_lowpass_filter(thorax_sensor_acc_y[i_:i_ + seq_length], 10, fc_, 4)

                                        # detect if it is right or left
                                        sensor_freq1, sensor_DF1 = find_freq_from_peaks(sensor_1F, fc_)
                                        sensor_freq2, sensor_DF2 = find_freq_from_peaks(sensor_2F, fc_)

                                        try:
                                            sensor_1_side = detect_RL_side(sensor_1F, sensor_2F, thorax_sensor_gyro_yF)
                                        except:
                                            here=0

                                        if sensor_1_side == 'R':
                                            # sensor 1 is the right foot
                                            # print(subj_, ' ', list(data_dict[subj_][cond_].keys())[feet_idx[0]], ' is the RIGHT foot')
                                            freq1 = sensor_freq1
                                            DF1 = sensor_DF1
                                            freq2 = sensor_freq2
                                            DF2 = sensor_DF2
                                        else:
                                            # sensor 2 is the right foot
                                            # print(subj_, ' ', list(data_dict[subj_][cond_].keys())[feet_idx[0]], ' is the LEFT foot')
                                            freq1 = sensor_freq2
                                            DF1 = sensor_DF2
                                            freq2 = sensor_freq1
                                            DF2 = sensor_DF1

                                        output_clear = False
                                        if not np.isnan([freq1, DF1, freq2, DF2]).any():
                                            output_clear = True
                                        else:
                                            print('NAN in the output!!!!')

                                    else:
                                        # old version with movesense
                                        y_dataset.append(labels[i_:i_ + seq_length])
                                else:
                                    pass

                                if input_clear and output_clear:
                                    y_dataset.append([freq1, DF1, freq2, DF2])
                                    X_dataset.append(xN)

                            print('File with ', sensors_, ' in ', cond_, ' for ', subj_, ' -> COMPLETED')

                    print('Condition completed. Input shape ->', np.shape(X_dataset))
                except Exception as e:
                    print(e)
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                    print(exc_type, fname, exc_tb.tb_lineno)
                    print('Bad file with ', sensors_, ' in ', cond_, ' for ', subj_, ' -> SKIPPING')

        return X_dataset, y_dataset

    def balance(self, x, y):
        x_balanced = []
        y_balanced = []

        tmp_x = []
        tmp_y = []
        for i in range(len(y)):
            if np.mean(y[i])<1:
                x_balanced.append(x[i])
                y_balanced.append(y[i])
            else:
                tmp_x.append(x[i])
                tmp_y.append(y[i])
        return x_balanced, y_balanced
        #ds_ = list(zip(tmp_x, tmp_y))
        #random.shuffle(ds_)
        #tmp_x_shuffled, tmp_y_shuffled = list(zip(*ds_))
        #x_balanced.extend(tmp_x_shuffled[:len(x_balanced)])
        #y_balanced.extend(tmp_x_shuffled[:len(y_balanced)])




        hard_label = [np.unique(i, return_counts=True) for i in y]
        unique, counts = np.unique(np.unique(y), return_counts=True)
