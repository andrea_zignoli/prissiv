import os
import numpy as np
import matplotlib.pyplot as plt

class single_subject:

    def __init__(self, file_name):
        import json as js
        import configparser
        config = configparser.ConfigParser()
        config.read('config.ini')
        with open(file_name) as f:
            self.data = js.load(f)
            self.template = config['PLOT']['template']

    def print_trial_list(self):
        subject_name_key = list(self.data.keys())
        subject_trial_key = list(self.data[subject_name_key[0]].keys())
        print('Trial list: ', subject_trial_key)

    def print_structure(self):
        subject_name_key = list(self.data.keys())
        subject_trial_key = list(self.data[subject_name_key[0]].keys())
        print(subject_name_key[0])
        for i, key_ in enumerate(subject_trial_key):
            sensor_ID_key = list(self.data[subject_name_key[0]][key_].keys())
            print('\t|')
            print('\t|_', key_)

            if 'label' in sensor_ID_key:
                sensor_ID_key.remove('label')

            if 'initial_time_delay' in sensor_ID_key:
                sensor_ID_key.remove('initial_time_delay')

            for j, sensor_ID_key_ in enumerate(sensor_ID_key):
                if i < len(subject_trial_key) - 1:
                    print('\t|\t|')
                    print('\t|\t|_', sensor_ID_key_)
                else:
                    print('\t\t|')
                    print('\t\t|_', sensor_ID_key_)
                sensor_type_key = list(self.data[subject_name_key[0]][key_][sensor_ID_key_].keys())
                for k, sensor_type_key_ in enumerate(sensor_type_key):
                    if i < len(subject_trial_key) - 1:
                        # print('\t|\t|\t|_', sensor_type_key_)
                        if j < len(sensor_ID_key) - 1:
                            print('\t|\t|\t|_', sensor_type_key_)
                        else:
                            print('\t|\t\t|_', sensor_type_key_)
                    else:
                        # print('\t\t|\t|_', sensor_type_key_)
                        if j < len(sensor_ID_key) - 1:
                            print('\t\t|\t|_', sensor_type_key_)
                        else:
                            print('\t\t\t|_', sensor_type_key_)

    def IMU_plot(self, trial_name='', sensor_number=0):
        import plotly.graph_objects as go
        import plotly.io as pio
        from plotly.subplots import make_subplots

        template = self.template

        subject_name_key = list(self.data.keys())
        subject_trial_key = list(self.data[subject_name_key[0]].keys())
        if trial_name == '':
            trial_name = subject_trial_key[0]

        if sensor_number > 3:
            sensor_number = 0

        sensor_ID_list = list(self.data[subject_name_key[0]][trial_name].keys())

        selected_ID_sensor = sensor_ID_list[sensor_number]

        sensor_kind_list = list(self.data[subject_name_key[0]][trial_name][selected_ID_sensor].keys())
        sensor_coord_list = list(self.data[subject_name_key[0]][trial_name][selected_ID_sensor][sensor_kind_list[0]])
        if 'timestamp' in sensor_coord_list:
            sensor_coord_list.remove('timestamp')

        fig = make_subplots(rows=2, cols=2)
        for coord in sensor_coord_list:
            sensor_type = 'acc'
            time_array = self.data[subject_name_key[0]][trial_name]['time_array']
            y_data = self.data[subject_name_key[0]][trial_name][selected_ID_sensor][sensor_type][coord]
            fig.add_trace(go.Scatter(x=time_array,
                                     y=y_data,
                                     mode='lines',
                                     name=coord),
                          row=1,
                          col=1)
            fig.update_yaxes(title_text=sensor_type, row=1, col=1)
            sensor_type = 'gyro'
            y_data = self.data[subject_name_key[0]][trial_name][selected_ID_sensor][sensor_type][coord]
            fig.add_trace(go.Scatter(x=time_array,
                                     y=y_data,
                                     mode='lines',
                                     name=coord),
                          row=1,
                          col=2)
            fig.update_yaxes(title_text=sensor_type, row=1, col=2)
            sensor_type = 'magn'
            y_data = self.data[subject_name_key[0]][trial_name][selected_ID_sensor][sensor_type][coord]
            fig.add_trace(go.Scatter(x=time_array,
                                     y=y_data,
                                     mode='lines',
                                     name=coord),
                          row=2,
                          col=1)
            fig.update_yaxes(title_text=sensor_type, row=2, col=1)

        if 'heartRate' in sensor_kind_list:
            sensor_type = 'heartRate'
            y_data = self.data[subject_name_key[0]][trial_name][selected_ID_sensor][sensor_type]['average']
            fig.add_trace(go.Scatter(x=time_array,
                                     y=y_data,
                                     mode='lines',
                                     name=sensor_type,
                                     yaxis='y1'),
                          row=2,
                          col=2)
            fig.update_yaxes(title_text=sensor_type, row=2, col=2, secondary_y=False)
            y_data = self.data[subject_name_key[0]][trial_name][selected_ID_sensor][sensor_type]['rrData']
            fig.add_trace(go.Scatter(x=time_array,
                                     y=y_data,
                                     mode='lines',
                                     name='RR',
                                     yaxis='y2'),
                          row=2,
                          col=2)
            fig.update_layout(yaxis=dict(side='right', title='HR'),
                              yaxis1=dict(side='left', title='RR'))
            fig.update_yaxes(title_text=sensor_type, row=2, col=2)

        fig.update_layout(title='Sensor output',
                          template=template)
        fig.show()

    def optogait_plot(self, trial_name=''):
        import plotly.graph_objects as go
        import plotly.io as pio
        from plotly.subplots import make_subplots

        template = self.template

        subject_name_key = list(self.data.keys())
        subject_trial_key = list(self.data[subject_name_key[0]].keys())
        if trial_name == '':
            trial_name = subject_trial_key[0]

        selected_ID_sensor = 'Optogait'

        variable_list = list(self.data[subject_name_key[0]][trial_name][selected_ID_sensor].keys())

        time_array = self.data[subject_name_key[0]][trial_name]['time_array']
        y_data = np.asarray(self.data[subject_name_key[0]][trial_name][selected_ID_sensor]['ContactTime'])

        fig = go.Figure()
        fig.add_trace(go.Scatter(x=time_array,
                                 y=y_data,
                                 mode='lines',
                                 name='ContactTime'))
        fig.update_layout(title='Optogait',
                          template=template,
                          yaxis_range=[0, 0.5])
        fig.show()

    def all_signals_plot(self, trial_name='', right_foot_sensor=1, left_foot_sensor=0):

        import plotly.graph_objects as go
        import pandas as pd
        import numpy as np
        from scipy.signal import butter, filtfilt

        def butter_lowpass_filter(data, cutoff, fs, order):
            normal_cutoff = cutoff / nyq
            # Get the filter coefficients
            b, a = butter(order, normal_cutoff, btype='low', analog=False)
            y = filtfilt(b, a, data)
            return y

        # Filter requirements.
        fs = 208  # sample rate, Hz
        cutoff = 4  # desired cutoff frequency of the filter, Hz ,      slightly higher than actual 1.2 Hz
        nyq = 0.5 * fs  # Nyquist Frequency
        order = 2  # sin wave can be approx represented as quadratic
        template = self.template

        subject_name_key = list(self.data.keys())
        subject_trial_key = list(self.data[subject_name_key[0]].keys())

        if trial_name == '':
            trial_name = subject_trial_key[0]

        sensor_ID_list = list(self.data[subject_name_key[0]][trial_name].keys())
        selected_ID_sensor = sensor_ID_list[right_foot_sensor]

        time_array = np.array(self.data[subject_name_key[0]][trial_name][selected_ID_sensor]['gyro']['timestamp'])
        time_series = pd.Series((time_array - time_array[0])/1000)
        time_series = time_series.interpolate()
        time_array = time_series.values - self.data[subject_name_key[0]][trial_name]['initial_time_delay']
        y_data = self.data[subject_name_key[0]][trial_name][selected_ID_sensor]['acc']['z']
        # y_data = [0 if x > 0 else x for x in y_data]
        yF1 = butter_lowpass_filter(y_data, cutoff, fs, order)

        fig = go.Figure()

        fig.add_trace(go.Scatter(x=time_array,
                                 y=y_data,
                                 opacity=1,
                                 mode='lines',
                                 name='Raw first sensor'))
        fig.add_trace(go.Scatter(x=time_array,
                                 y=yF1,
                                 opacity=.25,
                                 mode='lines',
                                 name='Filtered first sensor'))

        selected_ID_sensor = sensor_ID_list[left_foot_sensor]
        time_array = np.array(self.data[subject_name_key[0]][trial_name][selected_ID_sensor]['gyro']['timestamp'])
        time_series = pd.Series((time_array - time_array[0])/1000)
        time_series = time_series.interpolate()
        time_array = time_series.values - self.data[subject_name_key[0]][trial_name]['initial_time_delay']
        # y_data = np.sqrt(np.power(self.data[subject_name_key[0]][trial_name][selected_ID_sensor]['acc']['z'], 2))
        y_data = self.data[subject_name_key[0]][trial_name][selected_ID_sensor]['acc']['z']
        # y_data = [0 if x > 0 else x for x in y_data]
        yF2 = butter_lowpass_filter(y_data, cutoff, fs, order)

        fig.add_trace(go.Scatter(x=time_array,
                                 y=y_data,
                                 opacity=1,
                                 mode='lines',
                                 name='Raw second sensor'))
        fig.add_trace(go.Scatter(x=time_array,
                                 y=yF2,
                                 opacity=.25,
                                 mode='lines',
                                 name='Filtered second sensor'))

        plot_title = 'Subject: ' + subject_name_key[0] + ' - Trial: ' + trial_name
        fig.update_layout(title=plot_title,
                          template=template
                          # yaxis_range=[-9.8*8, 9.8*8]
                          )

        y_data = self.data[subject_name_key[0]][trial_name]['label']
        fig.add_trace(go.Scatter(x=time_array,
                                 y=y_data,
                                 mode='lines',
                                 name='Optogait'))

        fig.show()
