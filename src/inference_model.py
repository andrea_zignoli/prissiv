import os
import tensorflow as tf
import numpy as np
import pandas as pd
from ModelCreator import MyModel
from ModelCreator import DenseModel
from DataSetPreparation import data_set_preparation
from os.path import join
from os import walk
import matplotlib.pyplot as plt
import configparser

# Read local file `config.ini`.
config = configparser.ConfigParser()
config.read('config.ini')

# file folder
file_path = config['DATAFILES']['file_path']
# output directory
out_dir = config['DATAFILES']['out_dir']

cls = config.getint('MODEL', 'cls')
thorax_only = config.getboolean('MODEL', 'Thorax')

if thorax_only:
    n_sensors = 3
else:
    n_sensors = 18

# time-series details
fc = config.getint('SIGNALS', 'fc')
time_seq = config.getfloat('MODEL', 'time_seq')
time_stride = config.getfloat('MODEL', 'time_stride')
seq_length = int(time_seq * fc)
stride_length = int(time_stride * fc)

# load the models from weights or model file
print('LOADING THE MODEL')
model_type = config['MODEL']['model_type']

if model_type == 'weights':
    model_weights = '../models/weights.ckpt'
    # initiate the model
    model = MyModel(cls, n_sensors)
    # model = DenseModel(cls, n_sensors)
    # model.built = True
    model.build([None, n_sensors, seq_length])
    model.loadModel(model_weights, cls, n_sensors)
    print('-- MODEL LOADED WITH WEIGHTS --')

if model_type == 'saved_model':
    model_dir = '../models/my_model'
    model = tf.keras.models.load_model(model_dir)

out_dir = 'outdir'
data_dir = join(os.pardir, out_dir)
dirpath, dirnames, filenames = next(walk(data_dir))
filenames = [i for i in filenames if i[-5:] == '.json']

# load json data dict
test_ds = data_set_preparation()
test_ds.load_json(dirpath, dirnames, [filenames[0]])

time_stride_validation = 0.1
X_test, y_test = test_ds.windowing(fc_=fc, time_seq_=time_seq, time_stride_=time_stride_validation, cls_=cls, thorax_only_=thorax_only, gyro_=True)

ds_test = list(zip(X_test, y_test))
X_test, y_test = list(zip(*ds_test))

# inference
acc_x = list()
acc_y = list()
acc_z = list()
estimation = list()
ground_truth = list()
f1 = list()
f1_true = list()
f2 = list()
f2_true = list()
d1 = list()
d1_true = list()
d2 = list()
d2_true = list()

for i in range(len(X_test)):
    x, y = np.array(X_test[i]), np.array(y_test[i])
    acc_x.extend(x[0, :])
    acc_y.extend(x[1, :])
    acc_z.extend(x[2, :])
    if model_type == 'weights':
        inference = model(x.reshape(1, n_sensors, seq_length), training=False).numpy()
    if model_type == 'saved_model':
        inference = model.predict(x.reshape(1, n_sensors, seq_length))
    estimation.extend(inference)
    f1.append(inference[0][0])
    f2.append(inference[0][1])
    d1.append(inference[0][2])
    d2.append(inference[0][3])
    f1_true.append(y[0])
    f2_true.append(y[1])
    d1_true.append(y[2])
    d2_true.append(y[3])
    ground_truth.extend([y])
    # plt.plot(inference[0, :] + 1.4, 'o')
    # plt.plot(y + 1.4, 'x')
    # plt.show()

# post process statistics
MSE = list()
for est, truth in zip(estimation, ground_truth):
    MSE.append(np.sqrt(np.power((np.abs(est - truth)), 2)))

dummy=1
