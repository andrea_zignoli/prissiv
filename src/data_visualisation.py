from SingleSubject import single_subject
from os.path import join
import configparser

# Read local file `config.ini`.
config = configparser.ConfigParser()
config.read('config.ini')

file_dir = config['DATAFILES']['out_dir']
file_name = 'Sujet3(FV).json'

SS = single_subject(file_name=join(file_dir, file_name))
SS.print_structure()
# SS.IMU_plot(trial_name='-2', sensor_number=3)
# SS.optogait_plot()

trial_name_list = ['-2', '2', '5', '-5', '-8', '8', '80', '90', '100', '110', '120']

for trial_names in ['2']:
    try:
        SS.all_signals_plot(trial_name=trial_names, right_foot_sensor=0, left_foot_sensor=3)
    except:
        pass
